package com.day02;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.Dbconnection;

public class JDBCDemo5 {
    public static void main(String[] args) {

        Connection con = Dbconnection.getConnection();
        PreparedStatement pst = null;

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter EmpId whose password you want to update: ");
        int empId = scan.nextInt();
        System.out.print("Enter new Password: ");
        String newPassword = scan.next();
        System.out.println();

        try {
            String qry = "update employee set password = ? where empId = ?";

            pst = con.prepareStatement(qry);

            pst.setString(1, newPassword);
            pst.setInt(2, empId);

            int result = pst.executeUpdate();

            if (result > 0) {
                System.out.println("Password Updated Successfully!!!");
            } else {
                System.out.println("Password Update Failed. Employee with given ID not found.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    pst.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
