package com.day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.Dbconnection;

public class JDBCDemo6 {

	public static void main(String[] args) {
		Connection con = Dbconnection.getConnection();
        PreparedStatement pst = null;

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter EmpId to delete: ");
        int empIdToDelete = scan.nextInt();
        System.out.println();

        try {
            String qry = "DELETE FROM employee WHERE empId = ?";

            pst = con.prepareStatement(qry);

            pst.setInt(1, empIdToDelete);

            int result = pst.executeUpdate();

            if (result > 0) {
                System.out.println("Record(s) Deleted Successfully!!!");
            } else {
                System.out.println("Record(s) Deletion Failed. Employee with specified ID not found.");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    pst.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}
