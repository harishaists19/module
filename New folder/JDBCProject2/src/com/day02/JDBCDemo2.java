package com.day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.Dbconnection;

public class JDBCDemo2 {
	public static void main(String[] args) {
		
		Connection con = Dbconnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter EmployeeId: ");
		int empId = scan.nextInt();
		System.out.println();
		
		
		try {		
			pst = con.prepareStatement("Select * from employee where empId = ?");
			pst.setInt(1, empId);
			rs = pst.executeQuery();			
			
			if (rs.next()) {				
				System.out.println("Employee Details");
				System.out.println("----------------");
				System.out.println("EmpId    : " + rs.getInt(1));
				System.out.println("EmpName  : " + rs.getString(2));
				System.out.println("Salary   : " + rs.getDouble(3));
				System.out.println("Gender   : " + rs.getString(4));
				System.out.println("LoginId  : " + rs.getString(5));

			} else {
				System.out.println("Employee Record Not Found!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
}

