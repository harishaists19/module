package com.day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.Dbconnection;

public class JBDCDemo4 {

	public static void main(String[] args) {
		Connection con = Dbconnection.getConnection();
		PreparedStatement pst = null;
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter EmpId   : ");
		int empId = scan.nextInt();
		System.out.print("Enter EmpName : ");
		String empName = scan.next();
		System.out.print("Enter Salary  : ");
		double salary = scan.nextDouble();
		System.out.print("Enter Gender  : ");
		String gender = scan.next();
		System.out.print("Enter LoginId : ");
		String loginId = scan.next();
		System.out.print("Enter Password: ");
		String password = scan.next();
		System.out.println();
		
		
		try {		
			String qry = "insert into employee values (?, ?, ?, ?, ?, ?)";
			
			pst = con.prepareStatement(qry);
			
			pst.setInt(1, empId);
			pst.setString(2, empName);
			pst.setDouble(3, salary);
			pst.setString(4, gender);
			pst.setString(5, loginId);
			pst.setString(6, password);
			
			int result = pst.executeUpdate();		
			
			if (result > 0) {				
				System.out.println("Record(s) Inserted Successfully!!!");
			} else {
				System.out.println("Record(s) Insertion Failed!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		

	}

}
