package Day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import Day01.DbConnection;

public class salary_filter {


	    public static void main(String[] args) {
	        Connection con = DbConnection.getConnection();
	        Statement st = null;
	        ResultSet rs = null;

	        if (con == null) {
	            System.out.println("DbConnection Failed!!!");
	            return;
	        }

	        System.out.println("Connection Established Successfully...");

	        filterBySalaryRange(con);
	    }

	    private static void filterBySalaryRange(Connection con) {
	        Scanner sc = new Scanner(System.in);

	        System.out.println("Salary Ranges");
	        System.out.println("-------------");
	        System.out.println("1. 1 to 3000 (< 3000)");
	        System.out.println("2. 3000 to 6000 (< 6000)");
	        System.out.println("3. 6000 to 9000 (< 9000)");
	        System.out.println("4. >= 9000");
	        System.out.print("Enter Your Choice: ");

	        int choice = sc.nextInt();

	        try (Statement st = con.createStatement()) {
	            String rangeFilter = "";
	            switch (choice) {
	                case 1:
	                    rangeFilter = "salary >= 1 AND salary < 3000";
	                    break;
	                case 2:
	                    rangeFilter = "salary >= 3000 AND salary < 6000";
	                    break;
	                case 3:
	                    rangeFilter = "salary >= 6000 AND salary < 9000";
	                    break;
	                case 4:
	                    rangeFilter = "salary >= 9000";
	                    break;
	                default:
	                    System.out.println("Invalid Choice!");
	                    return;
	            }

	            String query = "SELECT * FROM employee WHERE " + rangeFilter;
	            ResultSet countResult = st.executeQuery("SELECT COUNT(*) FROM employee WHERE " + rangeFilter);
	            if (countResult.next()) {
	                int count = countResult.getInt(1);
	                System.out.println("Output Count: " + count);

	                // Retrieve and print employee details
	                ResultSet employeeDetails = st.executeQuery(query);
	                while (employeeDetails.next()) {
	                    int empId = employeeDetails.getInt("empId");
	                    String empName = employeeDetails.getString("empName");
	                    String gender = employeeDetails.getString("gender");
	                    double empSalary = employeeDetails.getDouble("salary");

	                    // Print details in column-wise format
	                    System.out.println("Employee ID: " + empId);
	                    System.out.println("Employee Name: " + empName);
	                    System.out.println("Gender: " + gender);
	                    System.out.println("Salary: " + empSalary);
	                    System.out.println("--------------");
	                }
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	}