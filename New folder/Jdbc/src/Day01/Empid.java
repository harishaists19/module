package Day01;

import java.sql.SQLException;
import java.util.Scanner;

public class Empid {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
        Statement st = null;
        ResultSet rs = null;

        if (con == null) {
            System.out.println("DbConnection Failed!!!");
            return;
        }

        try {
            st = con.createStatement();
            rs = st.executeQuery("SELECT empId FROM employee");

            System.out.println("Available Employee IDs are:");
            System.out.println("-----------------------");
            while (rs.next()) {
                System.out.println(rs.getInt("empId"));
            }

            Scanner scan = new Scanner(System.in);
            System.out.print("\nEnter Employee Id: ");
            int empId = scan.nextInt();
            System.out.println();

            rs = st.executeQuery("SELECT * FROM employee WHERE empId = " + empId);

            if (rs.next()) {
                System.out.println("Employee Details");
                System.out.println("----------------");
                System.out.println("EmpId    : " + rs.getInt("empId"));
                System.out.println("EmpName  : " + rs.getString("empName"));
                System.out.println("Salary   : " + rs.getDouble("salary"));
                System.out.println("Gender   : " + rs.getString("gender"));
                System.out.println("LoginId  : " + rs.getString("loginId"));
            } else {
                System.out.println("Employee Record Not Found!!!");
            }

        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    rs.close();
                    st.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

	}

}
